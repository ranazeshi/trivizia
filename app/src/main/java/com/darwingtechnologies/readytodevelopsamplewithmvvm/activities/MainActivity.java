package com.darwingtechnologies.readytodevelopsamplewithmvvm.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.fragments.SplashFragment;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonObjects;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity  {
    private static MainActivity instance;
    public SpinKitView spin_kit;
    public static FragmentActivity currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spin_kit = findViewById(R.id.spin_kit);

        instance = this;

        CommonObjects.setContext(this);
        goForNextScreen();

    }

    private void goForNextScreen(){
        CommonMethods.callFragment(SplashFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out,this, false);
    }
    public static final boolean isInstanciated() {
        return instance != null;
    }

    public static final MainActivity instance() {
        if (instance != null) return instance;
        throw new RuntimeException("MainActivity not instantiated yet");
    }
    public boolean popBackStackMytm() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate();
//            currentFragment=getCurrentFragment();
            return true;
        }
        return false;
    }
    public void isShowSpinKit(Boolean isShow) {
        if (isShow) {
            spin_kit.setVisibility(View.VISIBLE);
        } else {
            spin_kit.setVisibility(View.GONE);
        }
    }
    public void checkAndRequestPermissionsToSendImage() {
        ArrayList<String> permissionsList = new ArrayList<>();

        int readExternalStorage =
                getPackageManager()
                        .checkPermission(
                                Manifest.permission.READ_EXTERNAL_STORAGE, getPackageName());
//        org.linphone.mediastream.Log.i(
//                "[Permission] Read external storage permission is "
//                        + (readExternalStorage == PackageManager.PERMISSION_GRANTED
//                                ? "granted"
//                                : "denied"));
        int camera =
                getPackageManager().checkPermission(Manifest.permission.CAMERA, getPackageName());
//        org.linphone.mediastream.Log.i(
//                "[Permission] Camera permission is "
//                        + (camera == PackageManager.PERMISSION_GRANTED ? "granted" : "denied"));

        int writeExternalStorage =
                getPackageManager()
                        .checkPermission(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE, getPackageName());
//        org.linphone.mediastream.Log.i(
//                "[Permission] Camera permission is "
//                        + (camera == PackageManager.PERMISSION_GRANTED ? "granted" : "denied"));

        if (readExternalStorage != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.READ_EXTERNAL_STORAGE);
//            org.linphone.mediastream.Log.i("[Permission] Asking for read external storage");
            permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA);
//            org.linphone.mediastream.Log.i("[Permission] Asking for camera");
            permissionsList.add(Manifest.permission.CAMERA);
        }
        if (writeExternalStorage != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//            org.linphone.mediastream.Log.i("[Permission] Asking for write external storage");
            permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionsList.size() > 0) {
            String[] permissions = new String[permissionsList.size()];
            permissions = permissionsList.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, 0);
        }
    }
    public void checkAndRequestPermissionsCallPhone() {
        ArrayList<String> permissionsList = new ArrayList<>();

        int call = getPackageManager().checkPermission(Manifest.permission.CALL_PHONE, getPackageName());

        if (call != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE);
//            org.linphone.mediastream.Log.i("[Permission] Asking for read external storage");
            permissionsList.add(Manifest.permission.CALL_PHONE);
        }

        if (permissionsList.size() > 0) {
            String[] permissions = new String[permissionsList.size()];
            permissions = permissionsList.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, 0);
        }
    }

}

package com.darwingtechnologies.readytodevelopsamplewithmvvm.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.adapters.PointsByCategoryListAdapter;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;

import java.util.ArrayList;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class PointsByCategoryFragment extends BaseFragment{
    private RecyclerView rvListItems;
    private ArrayList<String> categoryItems =new ArrayList<>();
    private ImageButton ibBack;
    private LinearLayout llEmptyList;


    @Override
    protected void initView() {
        ibBack = (ImageButton) mView.findViewById(R.id.ibBack);
        setIbBack(ibBack,"Points by Category");

        llEmptyList=mView.findViewById(R.id.llEmptyList);
        llEmptyList.setVisibility(View.GONE);

        rvListItems = mView.findViewById(R.id.rvListItems);
        rvListItems.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {
        loadCategories();
    }

    private void loadCategories(){
        categoryItems.add("Any Category");
        categoryItems.add("General Knowledge");
        categoryItems.add("Entertainment: Books");
        categoryItems.add("Entertainment: Film");
        categoryItems.add("Entertainment: Music");
        categoryItems.add("Entertainment: Musicals & Theatres");
        categoryItems.add("Entertainment: Television");
        categoryItems.add("Entertainment: Video Games");
        categoryItems.add("Entertainment: Board Games");
        categoryItems.add("Science & Nature");
        categoryItems.add("Science: Computers");
        categoryItems.add("Science: Mathematics");
        categoryItems.add("Mythology");
        categoryItems.add("Sports");
        categoryItems.add("Geography");
        categoryItems.add("History");
        categoryItems.add("Politics");
        categoryItems.add("Art");
        categoryItems.add("Celebrities");
        categoryItems.add("Animals");
        categoryItems.add("Vehicles");
        categoryItems.add("Entertainment: Comics");
        categoryItems.add("Science: Gadgets");
        categoryItems.add("Entertainment: Japanese Anime & Manga");
        categoryItems.add("Entertainment: Cartoon & Animations");
        rvListItems.setAdapter(new PointsByCategoryListAdapter(categoryItems));
        rvListItems.getAdapter().notifyDataSetChanged();

    }

    public static PointsByCategoryFragment newInstance() {
        PointsByCategoryFragment fragment = new PointsByCategoryFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.points_by_category_and_attempt_question_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return PointsByCategoryFragment.class.getSimpleName();
    }


}

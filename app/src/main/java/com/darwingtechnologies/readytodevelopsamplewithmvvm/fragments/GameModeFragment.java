package com.darwingtechnologies.readytodevelopsamplewithmvvm.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.handlers.AppHandler;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models.QuestionsResponse;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.PreferencesUtils;

import java.util.ArrayList;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class GameModeFragment extends BaseFragment{

    private Spinner spnrCategory,spnrDifficultyLevel,spnrTypeOfQuestion;
    private ArrayList<String> categoryItems =new ArrayList<>();
    private ArrayList<String> difficultyLevelItems =new ArrayList<>();
    private ArrayList<String> questionTypelItems =new ArrayList<>();
    private int categoryTypePosition = 0;
    private String selectedCategory="Any Category";
    private int difficultyLevelTypePosition = 0;
    private int questionTypePosition=0;

    private ImageButton ibBack;
    private TextView tvPlayNow;


    @Override
    protected void initView() {

        ibBack = (ImageButton) mView.findViewById(R.id.ibBack);
        setIbBack(ibBack,"Game Mode");

        spnrCategory = mView.findViewById(R.id.spnrCategory);
        spnrCategory.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        selectedCategory=categoryItems.get(i);
                        if(i>=1){
                            categoryTypePosition = (9+(i-1));
                        }else{
                            categoryTypePosition=0;
                        }
                        android.util.Log.e("selected position is ",""+selectedCategory);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });

        spnrDifficultyLevel = mView.findViewById(R.id.spnrDifficultyLevel);
        spnrDifficultyLevel.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        difficultyLevelTypePosition = i;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });

        spnrTypeOfQuestion = mView.findViewById(R.id.spnrTypeOfQuestion);
        spnrTypeOfQuestion.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        questionTypePosition = i;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });

        tvPlayNow=mView.findViewById(R.id.tvPlayNow);
        tvPlayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateUrlAndGetQuestionFromServer();
            }
        });
        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {


        loadCategories();
        loadDifficultyLevel();
        loadQuestionType();

    }

    private void loadCategories(){
        categoryItems.add("Any Category");
        categoryItems.add("General Knowledge");
        categoryItems.add("Entertainment: Books");
        categoryItems.add("Entertainment: Film");
        categoryItems.add("Entertainment: Music");
        categoryItems.add("Entertainment: Musicals & Theatres");
        categoryItems.add("Entertainment: Television");
        categoryItems.add("Entertainment: Video Games");
        categoryItems.add("Entertainment: Board Games");
        categoryItems.add("Science & Nature");
        categoryItems.add("Science: Computers");
        categoryItems.add("Science: Mathematics");
        categoryItems.add("Mythology");
        categoryItems.add("Sports");
        categoryItems.add("Geography");
        categoryItems.add("History");
        categoryItems.add("Politics");
        categoryItems.add("Art");
        categoryItems.add("Celebrities");
        categoryItems.add("Animals");
        categoryItems.add("Vehicles");
        categoryItems.add("Entertainment: Comics");
        categoryItems.add("Science: Gadgets");
        categoryItems.add("Entertainment: Japanese Anime & Manga");
        categoryItems.add("Entertainment: Cartoon & Animations");
        for (int i = 0; i <categoryItems.size() ; i++) {
            int earnPoints= PreferencesUtils.getInstance(mActivity).getInt(categoryItems.get(i),0);
            if(earnPoints!=0){
                android.util.Log.e(categoryItems.get(i)," Points: "+earnPoints);
            }
        }
        if (categoryItems !=null){
            setUpSpinner(spnrCategory, getDataforSpinner(categoryItems));
        }
        spnrCategory.setSelection(categoryTypePosition);

    }
    private void loadDifficultyLevel(){
        difficultyLevelItems.add("Any Difficulty");
        difficultyLevelItems.add("Easy");
        difficultyLevelItems.add("Medium");
        difficultyLevelItems.add("Hard");

        if (difficultyLevelItems !=null){
            setUpSpinner(spnrDifficultyLevel, getDataforSpinner(difficultyLevelItems));
        }
        spnrDifficultyLevel.setSelection(difficultyLevelTypePosition);
    }

    private void loadQuestionType(){
        questionTypelItems.add("Any Type");
        questionTypelItems.add("Multiple Choice");
        questionTypelItems.add("True / False");

        if (questionTypelItems !=null){
            setUpSpinner(spnrTypeOfQuestion, getDataforSpinner(questionTypelItems));
        }
        spnrTypeOfQuestion.setSelection(questionTypePosition);
    }
    public static GameModeFragment newInstance() {
        GameModeFragment fragment = new GameModeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.game_mode_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return GameModeFragment.class.getSimpleName();
    }

    private void setUpSpinner(Spinner spinner, String[] items) {
        try {
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mActivity, R.layout.item_spinner, items);
            dataAdapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
            spinner.setAdapter(dataAdapter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private String[] getDataforSpinner(ArrayList<String> items){
        int size = items.size();
        String[] itemsList = new String[size];

        for (int i = 0; i < items.size(); i++) {
            android.util.Log.e("item " + i, "" + items.get(i));
            itemsList[i] = String.valueOf(items.get(i));
        }
        return itemsList;
    }

    void generateUrlAndGetQuestionFromServer(){
        String generateUrl="?amount=10";
        if(categoryTypePosition!=0){
            generateUrl+="&category="+categoryTypePosition;
        }
        if(difficultyLevelTypePosition!=0){
            switch (difficultyLevelTypePosition){
                case 1:
                    generateUrl+="&difficulty=easy";
                    break;
                case 2:
                    generateUrl+="&difficulty=medium";
                    break;
                case 3:
                    generateUrl+="&difficulty=hard";
                    break;
            }
        }
        if(questionTypePosition!=0){
            switch (questionTypePosition){
                case 1:
                    generateUrl+="&type=multiple";
                    break;
                case 2:
                    generateUrl+="&type=boolean";
            }
        }
        AppHandler.getQuestions(generateUrl, new AppHandler.GetQuestionListener() {
            @Override
            public void onSuccess(QuestionsResponse questionResponse) {
                if(questionResponse!=null && questionResponse.getResults()!=null && questionResponse.getResults().size()>0){
                    CommonMethods.callFragment(QuizFragment.newInstance(selectedCategory,questionResponse.getResults()), R.id.flFragmentOverlay, 0, 0, mActivity, true);

                }else{
                    CommonMethods.displayMessagesDialog("No Questions found for your match, please try again...!",mActivity).show();
                }
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,error);
            }
        });
    }


}

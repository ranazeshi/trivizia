package com.darwingtechnologies.readytodevelopsamplewithmvvm.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.ButtonAnimations.MyBounceInterpolator;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonObjects;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
public class SplashFragment extends BaseFragment {
    private static int SPLASH_TIME_OUT = 1000;
    private Handler handler=new Handler();
    private Runnable runnable;

    @Override
    protected void initView() {

        final Animation animation= AnimationUtils.loadAnimation(mActivity,R.anim.bounceb);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        animation.setInterpolator(interpolator);
        mView.findViewById(R.id.ivAppLogo).setAnimation(animation);
        runnable=new Runnable() {
            @Override
            public void run() {
                goForNextScreen();

            }
        };


        CommonObjects.setContext(mActivity);
        handler.postDelayed(runnable, SPLASH_TIME_OUT);

    }

    @Override
    protected void loadData() {

    }

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.splash_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return SplashFragment.class.getSimpleName();
    }


    void goForNextScreen() {
      CommonMethods.callFragment(HomeFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, false);
    }

}

package com.darwingtechnologies.readytodevelopsamplewithmvvm.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.interfaces.AttemptUpdateListerner;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.interfaces.InterfaceHandler;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.interfaces.PointsUpdateListerner;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.interfaces.ProfileUpdateListerner;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.Constants;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.PreferencesUtils;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class HomeFragment extends BaseFragment implements PointsUpdateListerner, AttemptUpdateListerner, ProfileUpdateListerner {
    private LinearLayout llStatistics,llProfile,llSettings;
    private TextView tvName,tvPlay,tvViewAttempts,tvPointsByCategory;
    private TextView tvEarnPoints,tvAttempts;

    @Override
    protected void initView() {
        llStatistics=mView.findViewById(R.id.llStatistics);
        llProfile=mView.findViewById(R.id.llProfile);
        llSettings=mView.findViewById(R.id.llSettings);
        llStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.callFragment(StatisticsFragment.newInstance(), R.id.flFragmentOverlay, 0, 0, mActivity, true);
            }
        });
        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.callFragment(ProfileFragment.newInstance(), R.id.flFragmentOverlay, 0, 0, mActivity, true);
            }
        });

        llSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.showMessage(mActivity,"no settings available.");
            }
        });

        tvName=mView.findViewById(R.id.tvName);
        tvEarnPoints=mView.findViewById(R.id.tvEarnPoints);

        tvAttempts=mView.findViewById(R.id.tvAttempts);

        InterfaceHandler.setPointsUpdateListerner(this);
        InterfaceHandler.setAttemptUpdateListerner(this);
        InterfaceHandler.setProfileUpdateListerner(this);

        tvPlay=mView.findViewById(R.id.tvPlay);
        tvPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.callFragment(GameModeFragment.newInstance(), R.id.flFragmentOverlay, 0, 0, mActivity, true);
            }
        });

        tvViewAttempts=mView.findViewById(R.id.tvViewAttempts);
        tvViewAttempts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.callFragment(AttemptQuestionsFragment.newInstance(), R.id.flFragmentOverlay, 0, 0, mActivity, true);
            }
        });

        tvPointsByCategory=mView.findViewById(R.id.tvPointsByCategory);
        tvPointsByCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.callFragment(PointsByCategoryFragment.newInstance(), R.id.flFragmentOverlay, 0, 0, mActivity, true);
            }
        });

        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {
        tvName.setText(""+PreferencesUtils.getInstance(mActivity).getString(Constants.UserName,"You"));
        tvEarnPoints.setText(""+CommonMethods.getPoints(mActivity)+" Points");
        tvAttempts.setText("Attempts ("+CommonMethods.getAttempts(mActivity)+")");

    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.home_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return HomeFragment.class.getSimpleName();
    }


    @Override
    public void onPointsUpdateListerner(Boolean isUpdate) {
         if(isUpdate){
             tvEarnPoints.setText(""+CommonMethods.getPoints(mActivity)+" Points");
         }
    }

    @Override
    public void onAttemptsUpdation(Boolean isUpdate) {
        if(isUpdate){
            tvAttempts.setText("Attempts ("+CommonMethods.getAttempts(mActivity)+")");
        }
    }

    @Override
    public void onProfileUpdateListerner(Boolean isUpdate) {
        if(isUpdate){
            tvName.setText(""+PreferencesUtils.getInstance(mActivity).getString(Constants.UserName,"You"));
        }
    }
}

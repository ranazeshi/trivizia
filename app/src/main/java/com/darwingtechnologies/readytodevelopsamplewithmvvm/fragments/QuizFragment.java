package com.darwingtechnologies.readytodevelopsamplewithmvvm.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.SQLiteDB.QuestionsDbHelper;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models.QuestionLocalDBModel;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.interfaces.InterfaceHandler;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models.Result;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.Constants;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.PreferencesUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class QuizFragment extends BaseFragment implements View.OnClickListener{
    private LottieAnimationView lavCounDown;
    private LinearLayout llQuestionArea;
    private TextView tvQuestionOutOf,tvQuestion;
    private View layout_true_false,layout_four_option;
    private Button ansA, ansB, ansC, ansD, btnYes, btnNo;
    private static int queNumber=0;

    private List<Result> questions=new ArrayList<>();
    private String selectedCategory;
    private Result question;

    private ImageView ivIsCorrect;
    private Animation animation;

    private ImageView ivLeaveGame;
    private TextView tvEarnPoints;
    private int currentPoints=0;

    private QuestionsDbHelper questionsDbHelper;

    @Override
    protected void initView() {
        questionsDbHelper=new QuestionsDbHelper(getActivity());
        CommonMethods.updateAttempt(mActivity);
        InterfaceHandler.updateAttempt(true);

        ivLeaveGame=mView.findViewById(R.id.ivLeaveGame);
        ivLeaveGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.displayMessagesDialog("Are you sure you want to leave?",mActivity)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                goForNextScreen();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();

                            }
                        }).create().show();
            }
        });
        lavCounDown=mView.findViewById(R.id.lavCounDown);
        llQuestionArea=mView.findViewById(R.id.llQuestionArea);
        lavCounDown.setVisibility(View.VISIBLE);
        llQuestionArea.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lavCounDown.setVisibility(View.GONE);
                llQuestionArea.setVisibility(View.VISIBLE);
            }
        },2500);

        ivIsCorrect=mView.findViewById(R.id.ivIsCorrect);
        animation = AnimationUtils.loadAnimation(mActivity, R.anim.bounceb);

        tvQuestionOutOf=mView.findViewById(R.id.tvQuestionOutOf);
        tvQuestion=mView.findViewById(R.id.tvQuestion);

        layout_true_false=mView.findViewById(R.id.layout_true_false);
        layout_four_option=mView.findViewById(R.id.layout_four_option);

        ansA = mView.findViewById(R.id.ansA);
        ansB = mView.findViewById(R.id.ansB);
        ansC = mView.findViewById(R.id.ansC);
        ansD = mView.findViewById(R.id.ansD);


        btnYes = mView.findViewById(R.id.btnYes);
        btnNo = mView.findViewById(R.id.btnNo);

        setListener();

        tvEarnPoints=mView.findViewById(R.id.tvEarnPoints);
        tvEarnPoints.setText(""+CommonMethods.getPoints(mActivity)+" Points");


        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {
        getQuestionAndPopulateData();
    }

    public static QuizFragment newInstance(String selectedCategory,List<Result> result) {
        QuizFragment fragment = new QuizFragment();
        fragment.questions=result;
        fragment.selectedCategory=selectedCategory;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.quiz_screen_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return QuizFragment.class.getSimpleName();
    }

    private void setListener() {
        btnYes.setOnClickListener(this);
        btnNo.setOnClickListener(this);

        ansA.setOnClickListener(this);
        ansB.setOnClickListener(this);
        ansC.setOnClickListener(this);
        ansD.setOnClickListener(this);


//        txtQuestion.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                Utils.showLoger("txtRate onTextChanged " + s.length());
//                if (s.length() < 20) {
//                    txtQuestion.setTextSize(25);
//                } else if (s.length() >= 20 && s.length() < 50) {
//                    txtQuestion.setTextSize(20);
//                } else if (s.length() >= 50 && s.length() <= 70) {
//                    txtQuestion.setTextSize(18);
//                } else if (s.length() > 70 && s.length() <= 100) {
//                    txtQuestion.setTextSize(15);
//                } else {
//                    txtQuestion.setTextSize(12);
//                }
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

    }

    private void getQuestionAndPopulateData(){
        try {
            question = questions.get(queNumber);
            android.util.Log.e("queNumber:"+queNumber,""+new Gson().toJson(question));

            populateData();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void populateData() {
        if (question != null) {
            ivIsCorrect.setVisibility(View.GONE);

            setResetView();

            tvQuestionOutOf.setText((queNumber+1)+"/"+questions.size());
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                tvQuestion.setText(Html.fromHtml(question.getQuestion(),Html.FROM_HTML_MODE_LEGACY));
            }else{
                tvQuestion.setText(Html.fromHtml(question.getQuestion()));
            }
            if (question.getType() != null) {
                if (question.getType().equalsIgnoreCase("multiple")) {
                    layout_four_option.setVisibility(View.VISIBLE);
                    layout_true_false.setVisibility(View.GONE);
                } else if (question.getType().equalsIgnoreCase("boolean")) {
                    layout_four_option.setVisibility(View.GONE);
                    layout_true_false.setVisibility(View.VISIBLE);
                }

                setQuestionOption();
            }

        } else {
            CommonMethods.displayMessagesDialog("No Questions Available...!",mActivity).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    goForNextScreen();
                }
            }).create().show();

            layout_four_option.setVisibility(View.GONE);
            layout_true_false.setVisibility(View.GONE);
            llQuestionArea.setVisibility(View.GONE);
        }
    }

    private void setQuestionOption(){
        question.getIncorrectAnswers().add(question.getCorrectAnswer());
        Collections.shuffle(question.getIncorrectAnswers());

        if(question!=null && question.getType().equalsIgnoreCase("multiple")) {
            if (question.getIncorrectAnswers() != null && question.getIncorrectAnswers().size() >= 1 && question.getIncorrectAnswers().size() <= 4) {

                for (int i = 0; i < question.getIncorrectAnswers().size(); i++) {
                    switch (i) {
                        case 0:
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                ansA.setText("A: " + Html.fromHtml(question.getIncorrectAnswers().get(i),Html.FROM_HTML_MODE_LEGACY));
                            }else{
                                ansA.setText("A: " + Html.fromHtml(question.getIncorrectAnswers().get(i)));
                            }
                            break;
                        case 1:
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                ansB.setText("B: " + Html.fromHtml(question.getIncorrectAnswers().get(i),Html.FROM_HTML_MODE_LEGACY));
                            }else{
                                ansB.setText("B: " + Html.fromHtml(question.getIncorrectAnswers().get(i)));
                            }
                            break;
                        case 2:

                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                ansC.setText("C: " + Html.fromHtml(question.getIncorrectAnswers().get(i),Html.FROM_HTML_MODE_LEGACY));
                            }else{
                                ansC.setText("C: " + Html.fromHtml(question.getIncorrectAnswers().get(i)));
                            }
                            break;
                        case 3:
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                ansD.setText("D: " + Html.fromHtml(question.getIncorrectAnswers().get(i),Html.FROM_HTML_MODE_LEGACY));
                            }else{
                                ansD.setText("D: " + Html.fromHtml(question.getIncorrectAnswers().get(i)));
                            }
                            break;
                    }
                }
                return;
            }
        }

        ansA.setText("A");
        ansB.setText("B");
        ansC.setText("C");
        ansD.setText("D");
    }

    private void setResetView() {

//        remainingSeconds = 0;
        setButtonEnable(true);
        ansA.setBackgroundResource(R.drawable.button_rounded_true);
        ansB.setBackgroundResource(R.drawable.button_rounded_true);
        ansC.setBackgroundResource(R.drawable.button_rounded_true);
        ansD.setBackgroundResource(R.drawable.button_rounded_true);
    }

    private void setButtonEnable(boolean b) {
        btnYes.setEnabled(b);
        btnNo.setEnabled(b);
        ansA.setEnabled(b);
        ansB.setEnabled(b);
        ansC.setEnabled(b);
        ansD.setEnabled(b);

        if (b) {
            ansA.setAlpha(1.0f);
            ansB.setAlpha(1.0f);
            ansC.setAlpha(1.0f);
            ansD.setAlpha(1.0f);
            btnYes.setAlpha(1.0f);
            btnNo.setAlpha(1.0f);
        } else {

            ansA.setAlpha(.5f);
            ansB.setAlpha(.5f);
            ansC.setAlpha(.5f);
            ansD.setAlpha(.5f);
            btnYes.setAlpha(.5f);
            btnNo.setAlpha(.5f);

//            if (countDownTimer != null) {
//                countDownTimer.cancel();
//            }

        }
//        switch (PreferencesUtils.getInstance(mActivity).getInt(Constants.UserAnsOption+question.getId(), 5)) {
//            case 0:
//                ansA.setAlpha(1.0f);
//                break;
//            case 1:
//                ansB.setAlpha(1.0f);
//                break;
//            case 2:
//                ansC.setAlpha(1.0f);
//                break;
//            case 3:
//                ansD.setAlpha(1.0f);
//                break;
//        }


    }

    @Override
    public void onClick(View view) {

            switch (view.getId()) {
                case R.id.btnYes:
                    answerQuestion(0);
                    break;
                case R.id.btnNo:
                    answerQuestion(1);
                    break;
                case R.id.ansA:
                    answerQuestion(0);
                    break;
                case R.id.ansB:
                    answerQuestion(1);
                    break;
                case R.id.ansC:
                    answerQuestion(2);
                    break;
                case R.id.ansD:
                    answerQuestion(3);
                    break;
            }
        }

    private void answerQuestion(int i) {
        if (question != null) {
//            PreferencesUtils.getInstance(GamePlayActivity.this).putString(Constants.UserAnsId+question.getId(), question.getId());
            if(question.getIncorrectAnswers().get(i).equalsIgnoreCase(question.getCorrectAnswer())){
                PreferencesUtils.getInstance(mActivity).putBoolean(Constants.UserAnsOption+question.getQuestion(), true);
            }else {
                PreferencesUtils.getInstance(mActivity).putBoolean(Constants.UserAnsOption+question.getQuestion(), false);
            }
            setButtonEnable(false);

            questionsubmitted(i);
        }
    }

    private void questionsubmitted(int i){
        isCorrectAnswer(i);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(questions!=null && questions.size()>0 && question!=null){
                    if(++queNumber<questions.size()){
                        question=questions.get(queNumber);
                        android.util.Log.e("queNumber:"+queNumber,""+new Gson().toJson(question));
                        populateData();
                    } else {
                        queNumber=0;


                        String message = "Your Earned "+currentPoints+" points in this slot.";
                        CommonMethods.displayMessagesDialog(message,mActivity).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                             goForNextScreen();
                            }
                        }).create().show();
                    }
                }
            }
        },1000);
    }
    void isCorrectAnswer(int i){
        boolean isCorrectCheck=false;
        if (question.getType()!=null && (question.getType().equalsIgnoreCase("multiple") || question.getType().equalsIgnoreCase("boolean"))) {
            if(PreferencesUtils.getInstance(mActivity).getBoolean(Constants.UserAnsOption+question.getQuestion(), false)){
                android.util.Log.e("isCorrect:","true");
                ivIsCorrect.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_check_true_24));
                ivIsCorrect.setVisibility(View.VISIBLE);
                ivIsCorrect.startAnimation(animation);
                addPoint();
                isCorrectCheck=true;
            }else{
                android.util.Log.e("isCorrect:","false");
                ivIsCorrect.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_check_false));
                ivIsCorrect.setVisibility(View.VISIBLE);
                ivIsCorrect.startAnimation(animation);
                isCorrectCheck=false;
            }
        }

        if(questionsDbHelper!=null){
            QuestionLocalDBModel questionLocalDBModel=new QuestionLocalDBModel();
            questionLocalDBModel.setResult(question);
            questionLocalDBModel.setUserAnswer(question.getIncorrectAnswers().get(i));
            questionLocalDBModel.setCorrect(isCorrectCheck);
            questionsDbHelper.addQuestion(questionLocalDBModel);
            android.util.Log.e("Size",""+questionsDbHelper.rowcount());
        }
    }

    private void addPoint(){
        if(question.getDifficulty().equalsIgnoreCase("easy")){
            CommonMethods.addPoint(mActivity,1);
            currentPoints+=1;
        }else if(question.getDifficulty().equalsIgnoreCase("medium")){
            CommonMethods.addPoint(mActivity,2);
            currentPoints+=2;
        }else if(question.getDifficulty().equalsIgnoreCase("hard")){
            CommonMethods.addPoint(mActivity,3);
            currentPoints+=3;
        }

        int pointCat=PreferencesUtils.getInstance(mActivity).getInt(""+selectedCategory,0);
        PreferencesUtils.getInstance(mActivity).putInt(""+selectedCategory,(currentPoints+pointCat));

        tvEarnPoints.setText(""+CommonMethods.getPoints(mActivity)+" Points");
        InterfaceHandler.updatePoint(true);
    }

    void goForNextScreen() {
        CommonMethods.callFragment(HomeFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, false);
    }


}

package com.darwingtechnologies.readytodevelopsamplewithmvvm.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.interfaces.InterfaceHandler;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.Constants;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.PreferencesUtils;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class UpdateProfileFragment extends BaseFragment{
    private EditText etName;
    private TextView tvSaveUpdate;
    private ImageButton ibBack;
    @Override
    protected void initView() {
        ibBack = (ImageButton) mView.findViewById(R.id.ibBack);
        setIbBack(ibBack,"Update Profile");


        etName=mView.findViewById(R.id.etName);

        tvSaveUpdate=mView.findViewById(R.id.tvSaveUpdate);
        tvSaveUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etName!=null && etName.getText().toString()!=null && !etName.getText().toString().isEmpty()){
                    PreferencesUtils.getInstance(mActivity).putString(Constants.UserName,etName.getText().toString());
                    InterfaceHandler.updateProfile(true);
                    getIbBack().performClick();
                }else{
                    CommonMethods.showMessage(mActivity,"Please enter name to continue.");
                }
            }
        });

        CommonMethods.setupUI(mView,mActivity);
        CommonMethods.showSoftKeyboard(etName,mActivity);
    }

    @Override
    protected void loadData() {
        etName.setText(""+PreferencesUtils.getInstance(mActivity).getString(Constants.UserName,"You"));

    }

    public static UpdateProfileFragment newInstance() {
        UpdateProfileFragment fragment = new UpdateProfileFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.update_profile_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return UpdateProfileFragment.class.getSimpleName();
    }


}

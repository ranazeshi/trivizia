package com.darwingtechnologies.readytodevelopsamplewithmvvm.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.SQLiteDB.QuestionsDbHelper;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.adapters.AttemptQuestionsListAdapter;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models.QuestionLocalDBModel;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class AttemptQuestionsFragment extends BaseFragment{
    private RecyclerView rvListItems;
    private List<QuestionLocalDBModel> questions =new ArrayList<>();
    private ImageButton ibBack;
    private LinearLayout llEmptyList;

    @Override
    protected void initView() {
        ibBack = (ImageButton) mView.findViewById(R.id.ibBack);
        setIbBack(ibBack,"Attempt Questions");

        llEmptyList=mView.findViewById(R.id.llEmptyList);
        llEmptyList.setVisibility(View.GONE);
        rvListItems = mView.findViewById(R.id.rvListItems);
        rvListItems.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {
        loadAttemptQuestions();
    }

    private void loadAttemptQuestions(){
        questions=new QuestionsDbHelper(getActivity()).getAllQuestions();
        if(questions!=null && questions.size()>0) {
            rvListItems.setAdapter(new AttemptQuestionsListAdapter(questions));
            rvListItems.getAdapter().notifyDataSetChanged();
        }else{
            llEmptyList.setVisibility(View.VISIBLE);
        }
    }

    public static AttemptQuestionsFragment newInstance() {
        AttemptQuestionsFragment fragment = new AttemptQuestionsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.points_by_category_and_attempt_question_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return AttemptQuestionsFragment.class.getSimpleName();
    }


}

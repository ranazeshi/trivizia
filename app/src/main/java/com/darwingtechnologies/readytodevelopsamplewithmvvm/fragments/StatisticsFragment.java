package com.darwingtechnologies.readytodevelopsamplewithmvvm.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.SQLiteDB.QuestionsDbHelper;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models.QuestionLocalDBModel;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;

import java.util.List;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class StatisticsFragment extends BaseFragment{
    private TextView tvCorrectAnswers,tvIncorrectAnswers,tvTotalQuestions,tvAttempts,tvTotalPoints;
    private ImageButton ibBack;

    @Override
    protected void initView() {
        ibBack = (ImageButton) mView.findViewById(R.id.ibBack);
        setIbBack(ibBack,"Statistics");

        tvCorrectAnswers=mView.findViewById(R.id.tvCorrectAnswers);
        tvIncorrectAnswers=mView.findViewById(R.id.tvIncorrectAnswers);
        tvTotalQuestions=mView.findViewById(R.id.tvTotalQuestions);
        tvAttempts=mView.findViewById(R.id.tvAttempts);
        tvTotalPoints=mView.findViewById(R.id.tvTotalPoints);

        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

        int countingCorrect=0,inCorrectAnswers=0,totalQuestions=0;

        List<QuestionLocalDBModel> questions=new QuestionsDbHelper(getActivity()).getAllQuestions();
        if(questions!=null) {
            for (int i = 0; i < questions.size(); i++) {
                if (questions.get(i).getCorrect()) {
                    countingCorrect++;
                }
            }
            totalQuestions=questions.size();
            inCorrectAnswers=(totalQuestions-countingCorrect);
        }
        tvCorrectAnswers.setText(""+countingCorrect);
        tvIncorrectAnswers.setText(""+inCorrectAnswers);
        tvTotalQuestions.setText(""+totalQuestions);
        tvTotalPoints.setText(""+CommonMethods.getPoints(mActivity));
        tvAttempts.setText(""+CommonMethods.getAttempts(mActivity));

    }

    public static StatisticsFragment newInstance() {
        StatisticsFragment fragment = new StatisticsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.statistics_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return StatisticsFragment.class.getSimpleName();
    }


}

package com.darwingtechnologies.readytodevelopsamplewithmvvm.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.SQLiteDB.QuestionsDbHelper;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models.QuestionLocalDBModel;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.Constants;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.PreferencesUtils;

import java.util.List;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class ProfileFragment extends BaseFragment{
    private LinearLayout llUserInfoArea;
    private TextView tvName,tvEarnPoints,tvAttempts;
    private TextView tvCorrectAnswers,tvIncorrectAnswers,tvTotalQuestions,tvTotalPoints;
    private ImageButton ibBack;

    @Override
    protected void initView() {

        ibBack = (ImageButton) mView.findViewById(R.id.ibBack);
        setIbBack(ibBack,"Profile");

        llUserInfoArea=mView.findViewById(R.id.llUserInfoArea);
        llUserInfoArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.callFragment(UpdateProfileFragment.newInstance(), R.id.flFragmentOverlay, 0, 0, mActivity, true);
            }
        });

        tvName=mView.findViewById(R.id.tvName);
        tvEarnPoints=mView.findViewById(R.id.tvEarnPoints);

        tvAttempts=mView.findViewById(R.id.tvAttempts);

        tvCorrectAnswers=mView.findViewById(R.id.tvCorrectAnswers);
        tvIncorrectAnswers=mView.findViewById(R.id.tvIncorrectAnswers);
        tvTotalQuestions=mView.findViewById(R.id.tvTotalQuestions);
        tvTotalPoints=mView.findViewById(R.id.tvTotalPoints);

        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {
        tvName.setText(""+PreferencesUtils.getInstance(mActivity).getString(Constants.UserName,"You"));
        tvEarnPoints.setText(""+CommonMethods.getPoints(mActivity)+" Points");
        tvAttempts.setText("Attempts ("+CommonMethods.getAttempts(mActivity)+")");

        int countingCorrect=0,inCorrectAnswers=0,totalQuestions=0;

            List<QuestionLocalDBModel> questions=new QuestionsDbHelper(getActivity()).getAllQuestions();
            if(questions!=null) {
                for (int i = 0; i < questions.size(); i++) {
                    if (questions.get(i).getCorrect()) {
                        countingCorrect++;
                    }
                }
                totalQuestions=questions.size();
                inCorrectAnswers=(totalQuestions-countingCorrect);
            }
            tvCorrectAnswers.setText(""+countingCorrect);
            tvIncorrectAnswers.setText(""+inCorrectAnswers);
            tvTotalQuestions.setText(""+totalQuestions);
            tvTotalPoints.setText(""+CommonMethods.getPoints(mActivity));

    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.profile_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return ProfileFragment.class.getSimpleName();
    }


}

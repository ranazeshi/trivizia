package com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities;

public class Constants {
    public static String DEVICE_TYPE = "ANDROID";
    public static final String UserName= "UserName";
    public static final String UserAnsOption= "UserAnsOption";
    public static final String UserPoints= "UserPoints";
    public static final String UserAttempts= "UserAttempts";

}

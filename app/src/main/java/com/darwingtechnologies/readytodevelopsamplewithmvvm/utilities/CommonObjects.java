package com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities;

import android.content.Context;


public class CommonObjects {

    private static Context context;
    private static String url="https://news.ycombinator.com/jobs";

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        CommonObjects.context = context;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        CommonObjects.url = url;
    }
}

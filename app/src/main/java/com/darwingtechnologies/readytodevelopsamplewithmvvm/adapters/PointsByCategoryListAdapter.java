package com.darwingtechnologies.readytodevelopsamplewithmvvm.adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.PreferencesUtils;

import java.util.ArrayList;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
public class PointsByCategoryListAdapter extends RecyclerView.Adapter<PointsByCategoryListAdapter.ViewHolder> {

    private ArrayList<String> listItems;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvCategory,tvEarnPoints;
        private ViewHolder(View view) {
            super(view);
            mView = view;
            tvCategory=mView.findViewById(R.id.tvCategory);
            tvEarnPoints=mView.findViewById(R.id.tvEarnPoints);


        }
    }

    public PointsByCategoryListAdapter(ArrayList<String> listItems) {
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(context==null)
            context = parent.getContext();
        View view = CommonMethods.createView(context, R.layout.list_item_point_by_categories, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String title = listItems.get(position);

        try {
            holder.tvCategory.setText(title);
            int earnPoints= PreferencesUtils.getInstance(context).getInt(title,0);
            holder.tvEarnPoints.setText(""+earnPoints+" Points");


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }



}


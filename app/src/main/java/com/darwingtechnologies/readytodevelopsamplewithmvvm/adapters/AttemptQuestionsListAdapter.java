package com.darwingtechnologies.readytodevelopsamplewithmvvm.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models.QuestionLocalDBModel;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;

import java.util.List;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
public class AttemptQuestionsListAdapter extends RecyclerView.Adapter<AttemptQuestionsListAdapter.ViewHolder> {

    private List<QuestionLocalDBModel> listItems;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvQuestion,tvCorrectAnswer,tvAttemptedAnswer;
        private ImageView ivIsCorrect;

        private ViewHolder(View view) {
            super(view);
            mView = view;
            tvQuestion=mView.findViewById(R.id.tvQuestion);
            tvCorrectAnswer=mView.findViewById(R.id.tvCorrectAnswer);
            tvAttemptedAnswer=mView.findViewById(R.id.tvAttemptedAnswer);
            ivIsCorrect=mView.findViewById(R.id.ivIsCorrect);

        }
    }

    public AttemptQuestionsListAdapter(List<QuestionLocalDBModel> listItems) {
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(context==null)
            context = parent.getContext();
        View view = CommonMethods.createView(context, R.layout.list_item_attempt_questions, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final QuestionLocalDBModel questionLocalDBModel = listItems.get(position);

        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.tvQuestion.setText("Question: "+Html.fromHtml(questionLocalDBModel.getResult().getQuestion(),Html.FROM_HTML_MODE_LEGACY));
            }else{
                holder.tvQuestion.setText("Question: "+Html.fromHtml(questionLocalDBModel.getResult().getQuestion()));
            }

            holder.tvCorrectAnswer.setText(questionLocalDBModel.getResult().getCorrectAnswer());
            holder.tvAttemptedAnswer.setText(questionLocalDBModel.getUserAnswer());
            if(questionLocalDBModel.getCorrect()){
                holder.ivIsCorrect.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_baseline_check_true_24));
            }else{
                holder.ivIsCorrect.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_baseline_check_false));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }



}


package com.darwingtechnologies.readytodevelopsamplewithmvvm.interfaces;

public class InterfaceHandler {
    private static PointsUpdateListerner pointsUpdateListerner;
    private static AttemptUpdateListerner attemptUpdateListerner;
    private static ProfileUpdateListerner profileUpdateListerner;

    public static void setPointsUpdateListerner(PointsUpdateListerner pointsUpdateListerner) {
        InterfaceHandler.pointsUpdateListerner = pointsUpdateListerner;
    }
    public static void updatePoint(Boolean isUpdate) {
        if (pointsUpdateListerner!=null){
            pointsUpdateListerner.onPointsUpdateListerner(isUpdate);
        }
    }


    public static void setAttemptUpdateListerner(AttemptUpdateListerner attemptUpdateListerner) {
        InterfaceHandler.attemptUpdateListerner = attemptUpdateListerner;
    }
    public static void updateAttempt(Boolean isUpdate) {
        if (attemptUpdateListerner!=null){
            attemptUpdateListerner.onAttemptsUpdation(isUpdate);
        }
    }

    public static void setProfileUpdateListerner(ProfileUpdateListerner profileUpdateListerner) {
        InterfaceHandler.profileUpdateListerner = profileUpdateListerner;
    }
    public static void updateProfile(Boolean isUpdate) {
        if (profileUpdateListerner!=null){
            profileUpdateListerner.onProfileUpdateListerner(isUpdate);
        }
    }



}

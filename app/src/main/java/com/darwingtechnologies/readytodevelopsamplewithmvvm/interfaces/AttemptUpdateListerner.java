package com.darwingtechnologies.readytodevelopsamplewithmvvm.interfaces;


public interface AttemptUpdateListerner {
    void onAttemptsUpdation(Boolean isUpdate);
}

package com.darwingtechnologies.readytodevelopsamplewithmvvm.business.handlers;

import android.content.Context;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.BuildConfig;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.R;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.activities.MainActivity;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonObjects;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.Constants;
import com.google.gson.Gson;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class CallForServer {

    private String url;
    private OnServerResultNotifier onServerResultNotifier;
    private String NO_INTERNET = "No internet connection";
    private static String BASE_URL="https://opentdb.com/api.php";
    private static String API_BASE_URL = BASE_URL;
    private static OkHttpClient client;
    private static boolean isShown;


    public CallForServer(String url, OnServerResultNotifier onServerResultNotifier) {
        this.url = url;
        this.onServerResultNotifier = onServerResultNotifier;
    }

    public interface OnServerResultNotifier {
        public void onServerResultNotifier(boolean isError, String response);
    }

    //Load data from server
    public void callForServerGet() {
        if (CommonMethods.isNetworkAvailable(CommonObjects.getContext())) {
            AndroidNetworking.initialize(CommonObjects.getContext());
            OkHttpClient client = new OkHttpClient()
                    .newBuilder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .build();
            AndroidNetworking.initialize(CommonObjects.getContext(),client);
            ANRequest.GetRequestBuilder getRequestBuilder = AndroidNetworking.get(API_BASE_URL+url);
            Log.d("Service_URL", API_BASE_URL+url);
            getRequestBuilder.addHeaders("Accept", "application/json");
            getRequestBuilder.build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String jsonStr) {
                    onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                    Log.e("JSON",""+jsonStr);
                }

                @Override
                public void onError(ANError error) {
                    String err="Unable to get data from server";
                    if(error.getErrorBody()!=null)
                    {
                        err=error.getErrorBody();
                    }
                    onServerResultNotifier.onServerResultNotifier(true,err);
                    Log.e("Erorr ",""+err.toString());

                }
            });
        } else {
            onServerResultNotifier.onServerResultNotifier(true, NO_INTERNET);
            Log.e("No Internet ",""+NO_INTERNET.toString());

        }
    }





}

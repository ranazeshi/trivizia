
package com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuestionLocalDBModel {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("result")
    @Expose
    private Result result = null;
    @SerializedName("userAnswer")
    @Expose
    private String userAnswer = null;
    @SerializedName("isCorrect")
    @Expose
    private Boolean isCorrect;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public Boolean getCorrect() {
        return isCorrect;
    }

    public void setCorrect(Boolean correct) {
        isCorrect = correct;
    }
}

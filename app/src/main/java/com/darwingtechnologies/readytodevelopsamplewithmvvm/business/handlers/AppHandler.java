package com.darwingtechnologies.readytodevelopsamplewithmvvm.business.handlers;

import android.util.Log;

import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models.QuestionsResponse;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonMethods;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.utilities.CommonObjects;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Rana Zeshan on 27-Jun-19.
 */


public class AppHandler {



    public interface GetQuestionListener {
        public void onSuccess(QuestionsResponse questionResponse);
        public void onError(String error);
    }

    public static void getQuestions(String generatedUrl, final GetQuestionListener getQuestionListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        new CallForServer(
                generatedUrl,
                new CallForServer.OnServerResultNotifier() {
                    @Override
                    public void onServerResultNotifier(boolean isError, String response) {
                        Log.e("questionResponse", "" + response);
                        CommonMethods.hideProgressDialog();
                        if (!isError) {
                            try {
                                QuestionsResponse questionsResponse = new Gson().fromJson(response, QuestionsResponse.class);
                                if ( questionsResponse!=null && questionsResponse.getResponseCode()==0) {
                                    getQuestionListener.onSuccess(questionsResponse);
                                } else {
                                        getQuestionListener.onError("No Questions Found in your match...!");
                                }
                            }  catch (Exception e) {
                                getQuestionListener.onError(e.getMessage());
                            }
                        } else {
                            getQuestionListener.onError("Something went wrong, please try again.");
                        }
                    }
                }).callForServerGet();
    }


}
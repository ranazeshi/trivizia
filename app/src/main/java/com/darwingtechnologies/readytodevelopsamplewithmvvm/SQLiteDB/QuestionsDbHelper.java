package com.darwingtechnologies.readytodevelopsamplewithmvvm.SQLiteDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models.QuestionLocalDBModel;
import com.darwingtechnologies.readytodevelopsamplewithmvvm.business.models.Result;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rana Zeshi on 23-Dec-20.
 */

public class QuestionsDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "questionsDatabase";
    // table name
    private static final String TABLE_QUESTIONS = "questions";

    //Table Columns names
    private static final String KEY_ID= "id";
    private static final String KEY_QUESTION_JSON= "questionJson";
    private static final String KEY_USER_ANSWER= "userAnswer";
    private static final String KEY_WAS_CORRECT= "isCorrect";


    private SQLiteDatabase dbase;
    public Context CContext;

    public QuestionsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.CContext=context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        dbase=db;
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_QUESTIONS + " ( "
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_QUESTION_JSON+ " TEXT, "
                + KEY_USER_ANSWER+ " TEXT, "
                + KEY_WAS_CORRECT+" INTEGER )";

        db.execSQL(sql);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);
        onCreate(db);
    }

    // Adding
    public boolean addQuestion(QuestionLocalDBModel questionLocalDBModel) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_QUESTION_JSON,new Gson().toJson(questionLocalDBModel.getResult()));
        values.put(KEY_USER_ANSWER,questionLocalDBModel.getUserAnswer());
        values.put(KEY_WAS_CORRECT,questionLocalDBModel.getCorrect()?0:1);

        // Inserting Row
        long result=db.insert(TABLE_QUESTIONS, null, values);

        db.close();

        if(result==-1){
            return false;
        }else {
            return true;
        }
    }

    public List<QuestionLocalDBModel> getAllQuestions() {
        List<QuestionLocalDBModel> questionLocalDBModels =null;
// Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS;
        dbase=this.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);
// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            questionLocalDBModels= new ArrayList<QuestionLocalDBModel>();
            do {
                final QuestionLocalDBModel questionLocalDBModel = new QuestionLocalDBModel();
                questionLocalDBModel.setId(cursor.getInt(0));
                questionLocalDBModel.setResult(new Gson().fromJson(cursor.getString(1),Result.class));
                questionLocalDBModel.setUserAnswer(cursor.getString(2));
                questionLocalDBModel.setCorrect(cursor.getInt(3)==0?true:false);

                questionLocalDBModels.add(questionLocalDBModel);

            } while (cursor.moveToNext());

        }

        return questionLocalDBModels;
    }

    public int rowcount()
    {
        int row=0;
        String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        row=cursor.getCount();
        return row;
    }




}



# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Trivizia
* 0.0.1

### What has been done? ###

* add app logo
* add amazing UI
* add point 2 (in which user can choose Category, Difficulty Level & type of questions)
* add point 3 (Point system according to the requirements, also save points against categories and display in points screen)
* add point 4 (Implement local storage. display saved question in attemp questions screen with statuses)
* Navigations in Home Screen 
  * Statistics (stats of complete quiz app in which user can see their Total questions, Correct/incorrect answers, total earned points, total attempts)
  * Profile (user stats and profile detail like name and user can change their name by clicking on Name field)
  * Settings (Not availabel for now)
  * Play (play quiz by clicking on play button which will navigate to the game mode)
  * View Attempt Questions (view all attempt questions with status and correct answers)
  * View Points by category (view all points by category in list)
  * Game Mode Screen (User can select Category, Difficulty Level & Question Type form Game Mode which will be shown by clicking on Play Button)
  * Play Now (Start Game and answer the questions by clicking on play now button in Game Mode Screen)

### on going task in point 5
* working on point 5 feature (late due to office work routine) will be pushed today before 12 am.

### Who do I talk to? ###

* Zeshan Ashraf
* zeshanashraf829@gmail.com